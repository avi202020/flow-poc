package cliffberg.flow.analyzer.unittest;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 Unit tests: test each derived type of FlowTypeDescriptor against its required contract.
 In other words, verify that the various methods (isA, equals, etc.) all behave
 as required.

 The value type methods to be tested are:
	equals
	isIntrinsic
	canBeAssignedTo
	isA
	duplicate
	getCommonTypeDesc
	findSharedTypes

 The non-value type methods to be tested are:
 	equals

 The value type classes to be tested are:

	IntrinsicTypeDescriptor<Type extends ValueType, Node, TId extends Node>
	IntrinsicAliasTypeDescriptor<Type extends ValueType, Node, TId extends Node>
	EnumTypeDescriptor<Type extends ValueType, Node, TId extends Node>
	StructTypeDescriptor<Type extends ValueType, Node, TId extends Node>
	StructExtensionTypeDescriptor<Type extends ValueType, Node, TId extends Node>
	StructCompositeTypeDescriptor<Type extends ValueType, Node, TId extends Node>
	ArrayTypeDescriptor<Type extends ValueType, Node, TId extends Node>
 */
public class TestFlowTypeDescriptor {

	@Test
	public void test....() {
		assertEquals(...., ....result);
	}
}
