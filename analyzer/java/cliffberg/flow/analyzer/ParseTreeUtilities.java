package cliffberg.flow.analyzer;

import cliffberg.flow.parser.FlowParser.Qual_nameContext;

import static cliffberg.utilities.AssertionUtilities.*;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.ParserRuleContext;

import java.util.List;

public class ParseTreeUtilities<Node, TId> {

	/**
	 Return the first (and only) child of the specified node. If the node has no
	 children, or if the number of children is more than one, or if the child is not
	 of the specified type, then throw an exception.
	 */
	public static ParseTree getOnlyChild(ParseTree node, Class kind) throws Exception {
		if (node.getChildCount() != 1) throw new Exception("There are " + node.getChildCount() +
			" children; expected one");
		ParseTree child = node.getChild(0);
		if (! kind.isAssignableFrom(child.getClass())) throw new Exception(
			"Expected node to be a " + kind.getName() + ", but it is a " + child.getClass().getName());
		return child;
	}

	/**
	 Get node's parent, and its parent, and so on - until a node of the specified kind
	 is found, and then return that node. If none is found, return null.
	 If the starting node is of the specified kind, simply return it.
	 */
	public static ParseTree findParentOfKind(ParseTree node, Class kind) {
		for (;;) {
			if (node == null) return null;
			if (kind.isAssignableFrom(node.getClass())) return node;
			node = node.getParent();
		}
	}

	/*
	 Explicit type conversions. We use these methods for all conversions so
	 that they can be easily found and scrutinized. In this way, we keep
	 most of the implementation independent of the Antlr types, thereby
	 reducing dependence on a particular parse tree tool (even though it is
	 a very good one). TODO: See if these can be obviated using type parameters.
	 */

	public TId asTId(TerminalNode tnode) {
		return (TId)tnode;
	}

	public TerminalNode asTerminalNode(TId id) {
		assertIsA(id, TerminalNode.class);
		return (TerminalNode)id;
	}

	public Node asNode(ParserRuleContext ctx) {
		return (Node)ctx;
	}

	public ParseTree asParseTree(Node node) {
		return (ParseTree)node;
	}

	public List<TId> asTIds(List<TerminalNode> tnodes) {
		return (List<TId>)tnodes;
	}
}
