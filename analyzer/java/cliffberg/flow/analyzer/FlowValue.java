package cliffberg.flow.analyzer;

import cliffberg.symboltable.ValueType;
import cliffberg.symboltable.ConversionException;
import static cliffberg.utilities.AssertionUtilities.*;

import java.util.Map;

/**
 A compile-time container for static expression values.
 */
public abstract class FlowValue {

	FlowDataType type;

	public FlowValue(FlowDataType type) {
		this.type = type;
	}

	FlowDataType getType() { return this.type; }

	FlowTypeDescriptor getTypeDescriptor() {
		....
	}

	public boolean isA(Class flowValueClass) {
		return flowValueClass.isAssignableFrom(this.getClass());
	}

	public boolean automaticConversionToAllowed(FlowDataType convertTo) {
		return this.type.automaticConversionToAllowed(convertTo);
	}

	public boolean valueIsIntegral() {
		if (value == null) return false;
		return
			(value instanceof Byte) ||
			(value instanceof Short) ||
			(value instanceof Integer) ||
			(value instanceof Long);
	}

	public boolean isPositiveNumber() {
		if (value == null) return false;
		if (! (obj instanceof Number)) return false;
		if (valueIsIntegral()) {
				return ((Number)obj).longValue() > 0;
		} else {
			return ((Number)obj).doubleValue() > 0;
		}
	}

	public boolean isZero() {
		if (value == null) return false;
		if (! (obj instanceof Number)) return false;
		if (valueIsIntegral()) {
				return ((Number)obj).longValue() == 0;
		} else {
			return ((Number)obj).doubleValue() == 0;
		}
	}

	public boolean valueIsJavaIntCompatible() {
		if (value == null) return false;
		return
			valueIsIntegral() &&
			(((Number)obj).longValue() <= Integer.MAX_VALUE);
	}

	public boolean valueIsJavaLongCompatible() {
		if (value == null) return false;
		return
			valueIsIntegral() &&
			(((Number)obj).longValue() <= Long.MAX_VALUE);
	}

	/**
	 Perform an intrinsic type conversion. Ref LRM, "Type Conversions".
	 */
	public FlowValue convertTo(FlowDataType toType) throws ConversionException {

		if (this.getType() == toType) return this;
		switch (this.getType()) {
			case FlowInt: {
				switch (toType) {
					case FlowLongInt: return new FlowLongIntValue( (long)(((FlowIntValue)this).value) );
					case FlowUnsignedInt: return new FlowUnsignedIntValue( new Integer((((FlowIntValue)this).value)) );
					default: throw new ConversionException(this, toType);
				}
			}
			case FlowLongInt: throw new ConversionException(this, toType);
			case FlowUnsignedInt: {
				switch (toType) {
					case FlowLongInt: return new FlowLongIntValue(
							(long)
							(
								((FlowUnsignedIntValue)this).value.intValue()
							)
						);
					case FlowUnsignedLongInt: return new FlowUnsignedLongIntValue(
							new Long(
								(long)
								(
									((FlowUnsignedIntValue)this).value.intValue()
								)
							)
						);
					default: throw new ConversionException(this, toType);
				}
			}
			case FlowUnsignedLongInt: throw new ConversionException(this, toType);
			case FlowFloat: {
				switch (toType) {
					case FlowLongFloat: return new FlowLongFloatValue(
							(double)
							(
								((FlowFloatValue)this).value  // float
							)
						);
					default: throw new ConversionException(this, toType);
				}
			}
			case FlowLongFloat: throw new ConversionException(this, toType);
			case FlowBoolean: throw new ConversionException(this, toType);
			case FlowByte: throw new ConversionException(this, toType);
			case FlowGlyph: {
				switch (toType) {
					case FlowString: return new FlowStringValue(
							String.valueOf(
								((FlowGlyphValue)this).value  // char
							)
						);
					default: throw new ConversionException(this, toType);
				}
			}
			case FlowString: {
				/* Allowed conversions:
					string -> glyph (string must be of length 1).
				 */
				switch (toType) {
					case FlowGlyph: {
						String strval = ((FlowStringValue)this).value;
						if (strval.length() == 1) { // string is of length 1
							return new FlowGlyphValue(strval.charAt(0));
						} else {
							throw new ConversionException(this, toType);
						}
					}
					default: throw new ConversionException(this, toType);
				}
			}
			case FlowEnum: throw new ConversionException(this, toType);
			case FlowStruct: throw new ConversionException(this, toType);
			case FlowArray: throw new ConversionException(this, toType);
			case FlowConduitReference: throw new ConversionException(this, toType);
			case FlowObjectReference: throw new ConversionException(this, toType);
			default: throw new RuntimeException("Unrecognized flow value type: " + this);
		}
	}
}

class FlowIntValue extends FlowValue {
	int value;
	FlowIntValue(int value) {
		super(FlowDataType.FlowInt);
		this.value = value;
	}
	public int getInt() { return value; }
}

class FlowLongIntValue extends FlowValue {
	long value;
	FlowLongIntValue(long value) {
		super(FlowDataType.FlowLongInt);
		this.value = value;
	}
	public long getLong() { return value; }
}

class FlowUnsignedIntValue extends FlowValue {
	// See https://blogs.oracle.com/darcy/unsigned-integer-arithmetic-api-now-in-jdk-8
	Integer value;
	FlowUnsignedIntValue(Integer value) {
		super(FlowDataType.FlowUnsigndInt);
		if (value < 0) throw new RuntimeException("Value may not be negative");
		this.value = value;
	}
}

class FlowUnsignedLongIntValue extends FlowValue {
	Long value;
	FlowUnsignedLongIntValue(Long value) {
		super(FlowDataType.FlowUnsignedLongInt);
		if (value < 0) throw new RuntimeException("Value may not be negative");
		this.value = value;
	}
}

class FlowFloatValue extends FlowValue {
	float value;
	FlowFloatValue(float value) {
		super(FlowDataType.FlowFloat);
		this.value = value;
	}
}

class FlowLongFloatValue extends FlowValue {
	double value;
	FlowLongFloatValue(double value) {
		super(FlowDataType.FlowLongFloat);
		this.value = value;
	}
}

class FlowBooleanValue extends FlowValue {
	boolean value;
	FlowBooleanValue(boolean value) {
		super(FlowDataType.FlowBoolean);
		this.value = value;
	}
}

class FlowByteValue extends FlowValue {
	byte value;
	FlowByteValue(byte value) {
		super(FlowDataType.FlowByte);
		this.value = value;
	}
}

class FlowGlyphValue extends FlowValue {
	char value;
	char lowSurrogate = 0;
	FlowGlyphValue(char value) {
		super(FlowDataType.FlowGlyph);
		this.value = value;
	}
	FlowGlyphValue(char highSur, char lowSur) {
		super(FlowDataType.FlowGlyph);
		this.value = highSur;
		this.lowSurrogate = lowSur;
	}
}

class FlowStringValue extends FlowValue {
	String value;
	FlowStringValue(String value) {
		super(FlowDataType.FlowString);
		this.value = value;
	}
}

class FlowEnumValue extends FlowValue {
	String value;
	FlowEnumValue(String value) {
		super(FlowDataType.FlowEnum);
		this.value = value;
	}
}

class FlowStructValue extends FlowValue {
	Map<String, FlowValue> fields;
	FlowStructValue(Map<String, FlowValue> fields) {
		super(FlowDataType.FlowStruct);
		this.fields = fields;
	}
	void setField(String fieldName, FlowValue value) {
		fields.put(fieldName, value);
	}
}

class FlowArrayValue extends FlowValue {
	FlowValue[] elements;
	FlowArrayValue(FlowValue[] elements) {
		super(FlowDataType.FlowArray);
		this.elements = elements;
	}
}
