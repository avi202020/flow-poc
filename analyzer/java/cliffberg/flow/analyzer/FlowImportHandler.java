package cliffberg.flow.analyzer;

import cliffberg.symboltable.AnalyzerFactory;
import cliffberg.symboltable.NameScope;
import cliffberg.symboltable.CompilerState;
import cliffberg.symboltable.ImportHandlerBase;
import cliffberg.symboltable.NamespaceProcessorBase;

import static cliffberg.utilities.AssertionUtilities.*;

import java.util.Map;
import java.util.HashMap;
import java.io.File;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.FileNotFoundException;
import java.net.URL;

/**
 For retrieveing namespace source code from the location specified
 in a namespace 'bindings' file. (Ref. LRM, "Namespaces")
 */
public class FlowImportHandler extends ImportHandlerBase {

	private String bindingFileName;
	private Map<String, Binding> bindings = new HashMap<String, Binding>();
	
	public FlowImportHandler(AnalyzerFactory analyzerFactory, String bindingFileName) {
		super(analyzerFactory);
		this.bindingFileName = bindingFileName;
		
		try { parseBindingsFile(); }
		catch (FileNotFoundException ex1) {  // ignore
		}
		catch (Exception ex) {
			throw new Error(ex.getMessage(), ex);
		}
	}
	
	/**
	 
	 */
	public void processNamespace(String qualName, String internalName, boolean parseOnly)
	throws Exception {
		if (! internalName.startsWith("/")) internalName = '/' + internalName;
		String namespaceName = qualName + internalName;
		processNamespace(namespaceName, parseOnly);
	}
	
	@Override 
	public NameScope processNamespace(String namespace, CompilerState state, boolean parseOnly)
	throws Exception {
		
		System.out.println("------------Importing namespace " + namespace); // debug
		
		// Separate internal namespace part, if any.
		String baseName;
		String slashInternalName = "";
		int slashPos = namespace.indexOf('/');
		if (slashPos < 0) { // no slash - no internal namespace
			baseName = namespace;
		} else {
			baseName = namespace.substring(0, slashPos);
			slashInternalName = namespace.substring(slashPos);
		}

		// Locate the namespace file and create a Reader for it.
		
		Reader reader = null;
		Binding binding = bindings.get(baseName);
		if (binding == null) {
			// Look in current directory
			reader = new FileReader(namespace.replace('.', '/'));
		} else switch (binding.protocol) {
			case Http:
			case Https:
					// Create HTTP Reader.
					URL url = new URL(binding.location + slashInternalName);
					reader = new InputStreamReader(url.openConnection().getInputStream());
				break;
			case File:
					// Create File Reader.
					reader = new FileReader(binding.location + slashInternalName);
				break;
			case Maven:
					// Invoke Maven to retrieve the artifact and then obtain its file location.
					// Ref: https://maven.apache.org/shared/maven-invoker/usage.html
					// Create File Reader.
					throw new RuntimeException("Maven not supported yet");
					//String mavenArtifactFilePath = ....
					//reader = new FileReader(mavenArtifactFilePath);
				//break;
			default:
				throw new RuntimeException("Unrecognized protcol: " + binding.protocol);
		}
		
		// Recursively call the compiler on the file.
		
		NamespaceProcessorBase namespaceProcessor = getAnalyzerFactory().createNamespaceProcessor();
		return namespaceProcessor.processNamespace(reader, parseOnly);
	}
	
	/**
	 Ref. LRM, "Namespaces".
	 Syntax of each line:
	 	^'namespace' [a-zA-Z0-9_\.]+ '->' *$
	 */
	private void parseBindingsFile() throws Exception {
		FileReader r = new FileReader(bindingFileName);
		BufferedReader bfr = new BufferedReader(r);
		int lineNo = 0;
		for (;;) {
			lineNo++;
			String line = bfr.readLine();
			if (line == null) break;
			int posOfArrow = line.indexOf("->");
			assertThat(posOfArrow > 0, "Error at line " + lineNo + ": no -> found");
			String lhs = expandEnvVars(line.substring(0, posOfArrow).trim());
			String rhs = expandEnvVars(line.substring(posOfArrow+2).trim());
			bindings.put(lhs, new Binding(rhs));
		}
	}
	
	private static class Binding {
		Protocol protocol;
		String location;
		Binding(Protocol protocol, String location) throws Exception {
			//this.protocol = Protocol.valueOf(protocolStr);
			init(protocol, location);
		}
		Binding(String uri) throws Exception {
			Protocol protocol;
			String path;
			int colonLoc = uri.indexOf(':');
			if (colonLoc < 0) { // no scheme - assume a file path
				protocol = Protocol.File;
				path = uri;
			} else {
				String scheme = uri.substring(0, colonLoc);
				if (scheme.equals("http")) protocol = Protocol.Http;
				else if (scheme.equals("https")) protocol = Protocol.Https;
				else if (scheme.equals("maven")) protocol = Protocol.Maven;
				else throw new Exception("Unrecognized scheme: " + scheme);
				
				// Find first non-slash char.
				int pathStart = colonLoc+1;
				for (;;) {
					if (pathStart >= uri.length()) throw new Exception(
						"Improper binding");
					if (uri.charAt(pathStart) == '/') pathStart++;
					else break;
				}
				path = uri.substring(pathStart);
			}
			init(protocol, path);
		}
		
		private void init(Protocol protocol, String location) {
			this.protocol = protocol;
			this.location = location;
		}
	}
	
	private static enum Protocol {
		Http,
		Https,
		File,
		Maven;
	}
	
	private static String expandEnvVars(String text) {        
		Map<String, String> envMap = System.getenv();       
		text = text.replaceAll("~", System.getProperty("user.home"));
		for (String key : envMap.keySet()) {
			String value = envMap.get(key);
			text = text.replaceAll("\\$\\{" + key + "\\}", value);
		}
		return text;
	}
}
