package cliffberg.flow.test.examples;

import cliffberg.flow.test.TestBase;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

import cucumber.api.Format;
import cucumber.api.java.Before;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ExpressionParser extends TestBase {

	@Given("^an expression parser program$")
	public void an_expression_parser_program(String source) throws Throwable {
		setSource(source);
	}
	
	@Then("^the expression test is parsed without error$")
	public void the_expression_test_is_parsed_without_error() throws Exception {
		checkParse();
	}
		
	@Then("^the expression test analyzes without an error$")
	public void the_expression_test_analyzes_without_an_error() throws Exception {
		checkAnalyze();
	}
		
	/*
	@Then("^the expression test generates ....$")
	public void the_expression_test_generates_....() throws Exception {
		checkGenerate();
		....
	}
	*/
}
