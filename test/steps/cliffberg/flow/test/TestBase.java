package cliffberg.flow.test;

import cliffberg.flow.parser.FlowLexer;
import cliffberg.flow.parser.FlowParser;
import cliffberg.flow.analyzer.FlowAnalyzerFactory;
import cliffberg.flow.generator.FlowGenerator;
import cliffberg.flow.main.Main;
import static cliffberg.flow.analyzer.FlowSymbolWrapper.*;

import cliffberg.symboltable.CompilerState;
import cliffberg.symboltable.AnalyzerFactory;
import cliffberg.symboltable.NameScope;

import static cliffberg.utilities.AssertionUtilities.*;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.misc.Interval;

import net.bytebuddy.dynamic.DynamicType;

import java.util.Map;
import java.util.List;
import java.util.stream.Stream;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.FileVisitResult;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.function.Supplier;

import cucumber.api.java.Before;
import cucumber.api.java.After;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 Utilities shared by the Flow Cucumber test suite.
 Ref for JVM hooks: http://zsoltfabok.com/blog/2012/09/cucumber-jvm-hooks/
 Ref for Antlr: http://www.antlr.org/api/Java/index.html
 */
public abstract class TestBase {
	
	protected void setSource(String source) { this.source = source; }
	
	protected String getSource() { return this.source; }
	
	protected CompilerState<ParseTree, ParseTree, TerminalNode>
		getState() { return this.state; }
	
	protected FlowGenerator getGenerator() { return this.generator; }
	
	protected Map<String, DynamicType> getGeneratorMap() { return this.generatorMap; }
		
	protected TestBase() { }
	
	protected void checkParse() throws Exception {
		boolean parseOnly = true;
		boolean noGen = true;
		boolean omitStandard = true;
		int maxNoOfErrors = 1;
		compile(getSource(), parseOnly, noGen, omitStandard, maxNoOfErrors);
	}
	
	protected void checkAnalyze() throws Exception {
		boolean parseOnly = false;
		boolean noGen = true;
		boolean omitStandard = false;
		int maxNoOfErrors = 1;
		compile(getSource(), parseOnly, noGen, omitStandard, maxNoOfErrors);
	}
	
	protected void checkGenerate() throws Exception {
		boolean parseOnly = false;
		boolean noGen = false;
		boolean omitStandard = false;
		int maxNoOfErrors = 1;
		compile(getSource(), parseOnly, noGen, omitStandard, maxNoOfErrors);
	}
	
	/**
	 Invoke the parser that is being tested, using the specified source input.
	 */
	private void compile(String source, boolean parseOnly, boolean noGen, boolean omitStandard)
	throws Exception {
		
		Reader reader = new StringReader(source);
		
		Main<ParseTree, ParseTree, ParseTree, TerminalNode>
			main = Main.createMain(System.getProperty("user.dir"), parseOnly, 
				noGen, omitStandard, maxNoOfErrors);
		
		// Invoke the parser and analyzer.
		this.state = Main.createCompilerState();
		String bindingFilePath = System.getProperty("user.dir");
		AnalyzerFactory analyzerFactory = new FlowAnalyzerFactory(state, bindingFilePath);
		main.parseAndAnalyze(analyzerFactory, reader, parseOnly);
		
		if (! noGen) {
			// Generate each namespace.
			System.out.println("Generating...");
			ParseTreeWalker walker = new ParseTreeWalker();
			this.generator = new FlowGenerator(state);
			for (org.antlr.v4.runtime.tree.ParseTree start : state.getASTs()) {
				org.antlr.v4.runtime.tree.ParseTree tree =
					(org.antlr.v4.runtime.tree.ParseTree)start;
				walker.walk(this.generator, tree);
			}
			this.generatorMap = generator.getClasses();
		}
	}
	
	protected void handleError(Supplier check) throws Exception {
		try { check.get(); }
		catch (Exception ex) {
			System.err.println(getSource());
			CompilerState<ParseTree, ParseTree, TerminalNode> state = getState();
			NameScope globalScope = state.getGlobalScope();
			globalScope.printDownward(System.err);
			List<ParseTree> trees = state.getASTs();
			System.err.println("ASTs (" + trees.size() + "):");
			/*
			for (ParseTree tree : trees) {
				printTree(tree, System.err, 0);
			}
			*/
			throw ex;
		}
	}
	
	private String source;
	
	private Main<ParseTree, ParseTree, ParseTree, TerminalNode> main;
	
	private CompilerState<ParseTree, ParseTree, TerminalNode> state;
			
	private FlowGenerator generator;
	
	private Map<String, DynamicType> generatorMap;
}
