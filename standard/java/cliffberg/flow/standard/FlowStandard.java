package cliffberg.flow.standard;

public class FlowStandard {
	public static final String PackageText =
	
		"namespace flow.Standard:\n" +
		"\n" +
		"type Exception: {\n" +
		"	constant message: string,\n" +
		"	constant cause: Exception\n" +
		"}\n" +
		"type UnexpectedException: Exception {}\n" +
		"type NoTarget: UnexpectedException {}\n" +
		"type NoActiveConduit: Exception {}\n" +
		"class Random {\n" +
		"	Random{}(seed: long float) native;\n" +
		"	method random() long float native;\n" +
		"}\n" +
		"function truncate(v: long float) long int native;\n" +
		"function exp(base, exponent: long float) long float native;\n"
		;
}
