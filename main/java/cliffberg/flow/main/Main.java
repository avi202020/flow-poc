package cliffberg.flow.main;

import cliffberg.flow.Config;
import cliffberg.flow.parser.FlowLexer;
import cliffberg.flow.parser.FlowParser;
import cliffberg.flow.analyzer.FlowSymbolWrapper;
import cliffberg.flow.analyzer.FlowAnalyzerFactory;
import cliffberg.flow.generator.FlowGenerator;
import cliffberg.flow.analyzer.FlowSymbolWrapper;
import cliffberg.flow.analyzer.FlowDataType;

import cliffberg.flow.standard.FlowStandard;

import cliffberg.symboltable.NameScope;
import cliffberg.symboltable.AnalyzerFactory;
import cliffberg.symboltable.CompilerState;
import cliffberg.symboltable.NamespaceProcessorBase;

import static cliffberg.utilities.AssertionUtilities.*;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

import java.util.List;
import java.util.Arrays;
import java.util.stream.Stream;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.FileReader;
import java.io.StringReader;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.FileVisitResult;
import java.nio.file.attribute.BasicFileAttributes;

public class Main<Type, Node, Start, AOidRef extends Node, TId extends Node> {

	public static String FlowCompilerVersion = 
	public static String FlowLanguageVersion = 
	
	private String bindingFilePath;
	private boolean parseOnly = false;
	private boolean noGen = false;
	private boolean tree = false;
	private boolean omitStandard = false;
	private CompilerState<ParseTree, ParseTree, TerminalNode> state;
	
	public static void main(String[] args) throws Exception {
		
		// Get arguments.
		// Ref: http://jopt-simple.github.io/jopt-simple/examples.html
		OptionParser parser = new OptionParser();
			//.withRequiredArg().required();
		
		parser.acceptsAll(Arrays.asList( "h", "help", "?" ), "show help" ).forHelp();
		parser.accepts("version", "Obtain the language version supported and the version of this compiler");
		parser.accepts("output", "Where to write the generated binary files");
		parser.accepts("bindings", "path of the bindings.flow file");
		parser.accepts("parseOnly", "whether to only parse, or also analyze and generate output");
		parser.accepts("noGen", "whether to generate output");
		parser.accepts("omitStandard", "whether to implicitly import the standard namespace");
		parser.accepts("tree", "whether to send the parse tree to stderr");
		parser.accepts("maxErrors", "Terminate after this number of errors; default=1");
		
		OptionSet options = null;
		try {
			options = parser.parse(args);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			parser.printHelpOn(System.out);
			System.exit(1);
		}
		
		List<?> filepaths = options.nonOptionArguments();
		
		if (options.has("version")) {
			if (options.hasArgument("version")) {
				parser.printHelpOn(System.err);
				System.exit(1);
			}
			System.out.println("Flow(TM) compiler version " + Config.FlowCompilerVersion + 
				"; compiles Flow(TM) language version " + Config.FlowLanguageVersion);
			if (filepaths.size() == 0) System.exit(0);
		}
		
		String outputDirStr;
		if (options.has("output")) {
			if (! options.hasArgument("output")) {
				parser.printHelpOn(System.err);
				System.exit(1);
			}
			outputDirStr = (String)(options.valueOf("output"));
		} else {
			outputDirStr = System.getProperty("user.dir");
		}
		
		String bindingFilePath;
		if (options.has("bindings")) {
			if (! options.hasArgument("bindings")) {
				parser.printHelpOn(System.err);
				System.exit(1);
			}
			bindingFilePath = (String)(options.valueOf("bindings"));
		} else {
			bindingFilePath = System.getProperty("user.dir");
		}
		
		boolean parseOnly = false;
		if (options.has("parseOnly")) {
			if (options.hasArgument("parseOnly")) {
				parser.printHelpOn(System.err);
				System.exit(1);
			}
			parseOnly = true;
		}
		
		boolean noGen = false;
		if (options.has("noGen")) {
			if (options.hasArgument("noGen")) {
				parser.printHelpOn(System.err);
				System.exit(1);
			}
			noGen = true;
		}
		
		boolean omitStandard = false;
		if (options.has("omitStandard")) {
			if (options.hasArgument("omitStandard")) {
				parser.printHelpOn(System.err);
				System.exit(1);
			}
			omitStandard = true;
		}
		
		boolean printParseTree = false;
		if (options.has("tree")) {
			if (options.hasArgument("tree")) {
				parser.printHelpOn(System.err);
				System.exit(1);
			}
			printParseTree = true;
		}
		
		int maxNoOfErrors = 1;
		if (options.has("maxErrors")) {
			if (! options.hasArgument("maxErrors")) {
				parser.printHelpOn(System.err);
				System.exit(1);
			}
			String maxNoOfErrorsStr = options.valueOf("maxErrors");
			try {
				maxNoOfErrors = Integer.parseInt(maxNoOfErrorsStr);
			} catch (Exception ex) {
				parser.printHelpOn(System.err);
				System.exit(1);
			}
		}
		
		Main <FlowDataType, ParseTree, ParseTree, ParseTree, TerminalNode>
			m = createMain(bindingFilePath, parseOnly, noGen, printParseTree,
				omitStandard, maaxNoOfErrors);

		for (Object filepath : filepaths) {
			assertIsA(filepath, String.class);
			
			// Invoke the parser and analyzer.
			try { m.parseAndAnalyze((String)filepath); }
			catch (Exception ex)
			{
				System.err.println(ex.getMessage());
				System.exit(1);
			}
			finally {
				for (;;) {
					CompilerState.ErrorMessage msg = this.state.getNextErrorMessage();
					if (msg == null) break;
					msg.print();
				}
				for (;;) {
					CompilerState.WarningMessage msg = this.state.getNextWarningMessage();
					if (msg == null) break;
					msg.print();
				}
				if (m.tree) {
					List<ParseTree> trees = m.getState().getASTs();
					System.err.println("ASTs (" + trees.size() + "):");
					for (ParseTree tree : trees) {
						FlowSymbolWrapper.printTree(tree, System.err, 0);
					}
				}
			}

			if (! m.noGen) {
				// Generate each namespace.
				System.out.println("Generating...");
				ParseTreeWalker walker = new ParseTreeWalker();
				FlowGenerator generator = new FlowGenerator(m.getState());
				for (ParseTree tree : m.getState().getASTs()) {
					walker.walk(generator, tree);
				}
				File outputDir = new File(outputDirStr);
				generator.write(outputDir);
			}
		}
		
		System.out.println("Done");
	}
	
	private Main(String bindingFilePath, boolean parseOnly, boolean noGen,
			boolean tree, boolean omitStandard, int maxNoOfErrors) {
		this.bindingFilePath = bindingFilePath;
		this.parseOnly = parseOnly;
		this.noGen = noGen;
		this.tree = tree;
		this.omitStandard = omitStandard;
		this.state = createCompilerState(maxNoOfErrors);
	}
	
	public static Main<FlowDataType, ParseTree, ParseTree, ParseTree, TerminalNode>
		createMain(String bindingFilePath, boolean parseOnly, boolean noGen, boolean tree,
			boolean omitStandard, int maxNoOfErrors) {
		
		return new Main<FlowDataType, ParseTree, ParseTree, ParseTree, TerminalNode>(
			bindingFilePath, parseOnly, noGen, tree, omitStandard, maxNoOfErrors);
	}
	
	public static CompilerState<FlowDataType, ParseTree, ParseTree, TerminalNode>
		createCompilerState(int maxNoOfErrors) {
		
		return new FlowCompilerState(maxNoOfErrors);
	}
	
	public CompilerState<FlowDataType, ParseTree, ParseTree, TerminalNode> getState() { return this.state; }
	
	/**
	 Main program calls this version.
	 */
	public void parseAndAnalyze(String filepath) throws Exception {
		
		Reader reader = new FileReader(filepath);
		parseAndAnalyze(getState(), reader);
	}
	
	/**
	 ImportHandlers should only call this version.
	 */
	public NameScope parseAndAnalyze(CompilerState state, Reader reader) throws Exception {
		return parseAndAnalyze(new FlowAnalyzerFactory<Type, Node, Start, AOidRef, TId>(
			state, this.bindingFilePath), reader, this.parseOnly);
	}
	
	/**
	 For use by other parseAndAnalyze methods. This method provides the common functionality.
	 This version can also be used by tests that need to provide their
	 own factory for creating ImportHandlers.
	 */
	public NameScope parseAndAnalyze(AnalyzerFactory analyzerFactory, Reader reader,
		boolean parseOnly) throws Exception {
		
		NamespaceProcessorBase namespaceProcessor = analyzerFactory.createNamespaceProcessor();
		
		System.out.println("Importing Standard...");
		if (! this.omitStandard) {
			Reader r = new StringReader(FlowStandard.PackageText);
			namespaceProcessor.processNamespace(r, parseOnly);
		}
		
		System.out.println("Processing the input...");
		NameScope nameScope = namespaceProcessor.processPrimaryNamespace(reader, parseOnly);
		
		return nameScope;
	}
}
